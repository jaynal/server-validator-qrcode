var express = require('express');
var router = express.Router();
var _db = require('./database').middleware;

/* GET users listing. */
router.get('/:couponId', _db, function (req, res, next) {
    req.db.Coupon.findOne({
        _id: req.params.couponId
    }).exec(function (err, c) {
        if (err) {
            return res.status(500).json({
                succes: false,
                data: err
            });
        }
        if (!c) {
            return res.status(404).json({
                succes: false,
                message: 'Coupon not found',
                data: err
            });
        }
        return res.status(200).json({
            succes: true,
            data: c
        })
    });
});

router.get('/', _db, function (req, res, next) {
    req.db.Coupon.find({}).exec(function (err, c) {
        if (err) {
            return res.status(500).json({
                succes: false,
                data: err
            });
        }
        return res.status(200).json({
            succes: true,
            data: c
        })
    });
});

router.post('/', _db, function (req, res, next) {
    console.log("Body sent: ", req.body)
    var coupon = new req.db.Coupon(req.body);
    coupon.save(function (err) {
        if (err) {
            // Error
            return res.status(500).json({
                succes: false,
                data: err
            });
        }
        return res.status(200).json({
            success: true,
            message: 'Coupon Created',
            data: coupon
        });
    });
});


router.put('/:couponId/validate', _db, function (req, res, next) {
    req.db.Coupon.update({
        _id: req.params.couponId,
        validated: false
    }, {
        $set: {
            validated: true
        }
    }).exec(function (err, c) {
        if (err) {
            return res.status(500).json({
                succes: false,
                data: err
            });
        }
        if (c.nModified == 0) {
            return res.status(404).json({
                succes: false,
                message: 'Coupon olready vaidated',
                data: c
            });
        }
        return res.status(200).json({
            succes: true,
            message: 'Coupon validated'
        })
    });
});

module.exports = router;
