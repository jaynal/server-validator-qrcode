// Database Manager
var mongoose = require('mongoose'),
    models = require('../models'),
    app = require('express')();

var dbUrl = "mongodb://admin:admin@ds011429.mlab.com:11429/nello";
try {
    var database = mongoose.createConnection(dbUrl);
} catch (e) {
    return log.error.app('Missing database connection url');
}

database.on('error', function (err) {
    console.log("Connection error: " + dbUrl + "\n" + err);
});
database.on('open', function () {
    console.log("Connection correct " + dbUrl + "\n");
});

var dbInstances = {
    Coupon: database.model('Coupon', models.Coupon, 'coupons')
};

// Db
exports.db = dbInstances;
// Database Middelware
exports.middleware = function _db(req, res, next) {
    req.db = dbInstances;
    return next();
};
