var mongoose = require('mongoose');
var Schema = mongoose.Schema;

exports.Coupon = new Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    created: {
        type: Date,
        default: new Date()
    },
    validated: {
        type: Boolean,
        default: false
    }
});
