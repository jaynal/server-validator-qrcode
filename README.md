# README #

* First Time

```
npm install
ppm start
```

after just **npm start**


##API

Download postman and import api from link: 

https://www.getpostman.com/collections/81a04f4988b042ac72ca

###API list

```

GET /coupon   (get all coupons)
GET /coupon/IDDELCOUPON  (get one coupon with _id param)
POST /coupon (create new coupon)
PUT /coupon/IDDELCOUPON/validate

```

## POST /coupon

coupon object params 

```
{
 "title": "il mio coupon",
 "description" : "Questo è il mio primo coupon"
}

```

default : 

```
"validated": true,
"created": new Date()
```

## The coupon identification param is:  **_id **
